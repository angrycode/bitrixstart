#!/bin/bash
# Start File.
echo 'START!'
echo 'Git Remove'
rm -rf ./.git

# Install bitrix
cd www/sites/s1

# Редакция Старт
wget -O bitrix.tar.gz https://www.1c-bitrix.ru/download/start_encode.tar.gz

# Редакция Стандарт
# wget -O bitrix.tar.gz  https://www.1c-bitrix.ru/download/standard_encode.tar.gz

# Редакция Малый бизнес
# wget -O bitrix.tar.gz  https://www.1c-bitrix.ru/download/small_business_encode.tar.gz

# Редакция Бизнес
# wget -O bitrix.tar.gz  https://www.1c-bitrix.ru/download/business_encode.tar.gz

tar -zxvf ./bitrix.tar.gz

rm -rf ./bitrix.tar.gz


echo 'Bitrix change file'
#rm -rf ./auth

mv .htaccess .htaccess.default
mv .htaccess_ac .htaccess

echo 'Bitrix change Dir'

mv bitrix ../../core/
mv upload ../../core/

echo 'Bitrix create link'
ln -s ../../core/bitrix bitrix
ln -s ../../local local
ln -s ../../core/upload upload


echo 'Install admin panel widget'
# got to www folder
cd ../..
mkdir ./core/bitrix/php_interface/
cp ./local/php_interface/this_site_logo.php ./core/bitrix/php_interface/

# WWW dir

cd data

#echo 'Create .ENV File'
#cp .env.example .env

echo 'Composer install'
php composer.phar install

echo 'Instal .env config'
./vendor/bin/jedi env:init default && cp ./bitrix/.settings_extra.php ../core/bitrix/ && cp ./bitrix/.settings.php ../core/bitrix/ &&  cp ./bitrix/php_interface/dbconn.php ../core/bitrix/php_interface/

echo 'Migrations install'
php migrator install

# SITE DIR
# cd ..
# cd sites
# cd s1


echo 'FINISH!'