<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

# Локализация
#use Bitrix\Main\Localization\Loc;
#Loc::loadMessages(__FILE__);
#echo Loc::getMessage("INTERVOLGA_TIPS.TITLE");

# Работа с GET- и POST-параметрами страницы
//use Bitrix\Main\Application;
//$request = Application::getInstance()->getContext()->getRequest();
//$name = $request->getPost("name");
//$email = htmlspecialchars($request->getQuery("email"));

// Работа с cookie
//use Bitrix\Main\Application;
//use Bitrix\Main\Web\Cookie;
//$cookie = new Cookie("TEST", 42);
//$cookie->setDomain("example.com");
//Application::getInstance()->getContext()->getResponse()->addCookie($cookie);
//// Cookie будет доступна только на следующем хите!
//echo Application::getInstance()->getContext()->getRequest()->getCookie("TEST");