<?php
//file /local/php_interface/include/functions.php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
// удяляем скрипты ядра при отдаче сайта пользователям
function deleteKernelJs(&$content) {
    global $USER, $APPLICATION;
    if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
    if($APPLICATION->GetProperty("save_kernel") == "Y") return;

    $arPatternsToRemove = Array(
        //паттерны для извлечения js
    );

    $content = preg_replace($arPatternsToRemove, "", $content);
    $content = preg_replace("/\n{2,}/", "\n\n", $content);
}

// удяляем css ядра при отдаче сайта пользователям
function deleteKernelCss(&$content) {
    global $USER, $APPLICATION;
    if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
    if($APPLICATION->GetProperty("save_kernel") == "Y") return;

    $arPatternsToRemove = Array(
        //паттерны для извлечения css
    );

    $content = preg_replace($arPatternsToRemove, "", $content);
    $content = preg_replace("/\n{2,}/", "\n\n", $content);
}

//Сжатие HTML
function ChangeMyContent(&$content)
{
	if(trim($input) === "") return $input;
	    // Remove extra white-space(s) between HTML attribute(s)
	    $input = preg_replace_callback('#<([^\/\s<>!]+)(?:\s+([^<>]*?)\s*|\s*)(\/?)>#s', function($matches) {
	        return '<' . $matches[1] . preg_replace('#([^\s=]+)(\=([\'"]?)(.*?)\3)?(\s+|$)#s', ' $1$2', $matches[2]) . $matches[3] . '>';
	    }, str_replace("\r", "", $input));
	    // Minify inline CSS declaration(s)
	    if(strpos($input, ' style=') !== false) {
	        $input = preg_replace_callback('#<([^<]+?)\s+style=([\'"])(.*?)\2(?=[\/\s>])#s', function($matches) {
	            return '<' . $matches[1] . ' style=' . $matches[2] . minify_css($matches[3]) . $matches[2];
	        }, $input);
	    }
	    if(strpos($input, '') !== false) {
	      $input = preg_replace_callback('#(.*?)#is', function($matches) {
	        return ''. minify_css($matches[2]) . '';
	      }, $input);
	    }
	    if(strpos($input, '') !== false) {
	      $input = preg_replace_callback('##is', function($matches) {
	        return '';
	      }, $input);
	    }
	$content = preg_replace(
	        array(
	            // t = text
	            // o = tag open
	            // c = tag close
	            // Keep important white-space(s) after self-closing HTML tag(s)
	            '#<(img|input)(>| .*?>)#s',
	            // Remove a line break and two or more white-space(s) between tag(s)
	            '#()|(>)(?:\n*|\s{2,})(<)|^\s*|\s*$#s',
	            '#()|(?)\s+(<\/.*?>)|(<[^\/]*?>)\s+(?!\<)#s', // t+c || o+t
	            '#()|(<[^\/]*?>)\s+(<[^\/]*?>)|(<\/.*?>)\s+(<\/.*?>)#s', // o+o || c+c
	            '#()|(<\/.*?>)\s+(\s)(?!\<)|(?)\s+(\s)(<[^\/]*?\/?>)|(<[^\/]*?\/?>)\s+(\s)(?!\<)#s', // c+t || t+o || o+t -- separated by long white-space(s)
	            '#()|(<[^\/]*?>)\s+(<\/.*?>)#s', // empty tag
	            '#<(img|input)(>| .*?>)<\/\1>#s', // reset previous fix
	            '#( ) (?![<\s])#', // clean up ...
	            '#(?<=\>)( )(?=\<)#', // --ibid
	            // Remove HTML comment(s) except IE comment(s)
	            '#\s*\s*|(?)\n+(?=\<[^!])#s'
	        ),
	        array(
	            '<$1$2',
	            '$1$2$3',
	            '$1$2$3',
	            '$1$2$3$4$5',
	            '$1$2$3$4$5$6$7',
	            '$1$2$3',
	            '<$1$2',
	            '$1 ',
	            '$1',
	            ""
	        ),
	    $input);
}
