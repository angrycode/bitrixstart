<?php
//file /local/php_interface/include/handlers.php
$eventManager = \Bitrix\Main\EventManager::getInstance();
addEventHandler("main", "OnEndBufferContent", "deleteKernelJs");

// удяляем css ядра при отдаче сайта пользователям
$eventManager->addEventHandler("main", "OnEndBufferContent", "deleteKernelCss");

// удяляем css ядра при отдаче сайта пользователям
$eventManager->addEventHandler("main", "OnEndBufferContent", "ChangeMyContent");