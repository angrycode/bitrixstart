

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Техническое обслуживание</title>
    <style>
  body { text-align: center; padding: 150px; }
  h1 { font-size: 50px; }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; width: 650px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>
</head>
<body>
<article>
    <h1>Мы скоро вернемся!</h1>
    <div>
        <p>Приносим извинения за неудобства, но в настоящее время мы проводим техническое обслуживание. Очень скоро мы вернемся в онлайн!</p>
        <p>&mdash; Спасибо</p>
    </div>
</article>
</body>
</html>