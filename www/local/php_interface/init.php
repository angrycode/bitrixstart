<?php

## ANGRYCODE.kz
##
####
#### Добавить пакет с помощью composer, заходим в папку /local/ и выполняем: php composer.phar require PACKNAME
####
#### Красивое распечатывание масива: dump_r($array)
####
##
##

use Bitrix\Main\Loader;
Loader::includeModule('iblock');

require_once $_SERVER['DOCUMENT_ROOT'] . '/../../data/vendor/autoload.php';

// Подключение обработчиков событий
if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/handlers.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/handlers.php");
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/functions.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/functions.php");
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/functions-dump.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/functions-dump.php");
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/functions-remove-default-scripts.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/functions-remove-default-scripts.php");
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/functions-ac.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/functions-ac.php");
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/migrations.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/migrations.php");
}

