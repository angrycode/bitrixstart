<style>
.adminpanelangrycode{
   margin: 10px;
   margin-bottom: 60px;
   padding: 10px;
   background: #fff;
   border-radius: 5px;
   border: 1px solid #a4b9cc;
   color: #666;
}

.adminpanelangrycode a{
   text-decoration:none;
   color: #ee4540;
}

.adminpanelangrycode a:hover{
   text-decoration:underline;
}

.adminpanelangrycode .adminpanelangrycode-1{
   text-align: center;
    width: 100%;
    display: block;
    margin-bottom: 10px;
    color: #666;
    /* text-decoration: none; */
    font-weight: 600;
}
</style>


<div class="adminpanelangrycode">
   <a style="display: inline-block;margin: 4px;padding: 0px 16px;"
      target="_blank" href="https://angrycode.kz/">
      <p class="adminpanelangrycode-1">сайт разработан в</p>
   <img style=" display: block; width: 100%;" src="https://angrycode.kz/local/templates/angrycode_light/assets/img/logo/angrycode.svg" title="" alt="WEB-Студия ANGRYCODE" /></a>

   <p style="text-align:center;font-size:14px;font-weight:bold;">Если вам нужно:</p>
   <ul>
      <li>Продление лицензии Битрикс</li>
      <li>Доработка текущего сайта</li>
      <li>Техническая поддержка</li>
      <li>Продвижение в интернете</li>
      <li>Разработка нового сайта</li>
      <li>Или другие вопросы по сайту</li>
   </ul>

   <p style="text-align:center;font-weight:600;">
      <a href="https://angrycode.bitrix24.kz/online/chat-support/" target="_blank">Напишите нам онлайн</a> либо свяжитесь любым удобным для вас способом<br><a href="https://angrycode.kz" taget="_blank">www.angrycode.kz</a>
   </p>
</div>

<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn-ru.bitrix24.kz/b1021979/crm/site_button/loader_4_0pj6un.js');
</script>