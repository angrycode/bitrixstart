<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATES_PATH . "/assets/dist/js/main.js");
Asset::getInstance()->addJs(SITE_TEMPLATES_PATH . "/assets/dist/js/vendor.js");
//
Asset::getInstance()->addCss(SITE_TEMPLATES_PATH."/assets/dist/styles/main.css");

?>

<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
<head>
    <!--    -->
    <title><?$APPLICATION->ShowTitle();?></title>
    <!--    -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#fff">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <!--    -->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <!--    -->
    <link rel="shortcut icon" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/favicon.ico" type="image/x-icon">
    <link rel="icon" sizes="16x16" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/favicon-16x16.png" type="image/png">
    <link rel="icon" sizes="32x32" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/favicon-32x32.png" type="image/png">
    <link rel="apple-touch-icon-precomposed" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-precomposed.png">
    <link rel="apple-touch-icon" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="167x167" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-167x167.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-180x180.png">
    <link rel="apple-touch-icon" sizes="1024x1024" href="<?=SITE_TEMPLATES_PATH?>/assets/dist/img/favicons/apple-touch-icon-1024x1024.png">
    <!--    -->
</head>
<body>
<!---->
<div id="panel">
    <?$APPLICATION->ShowPanel();?>
</div>
